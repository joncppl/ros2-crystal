pkgname=ros2-crystal
pkgver=patch5
pkgrel=0
arch=('x86_64')
url=https://index.ros.org/doc/ros2
license=('Apache2')
depends=(
'python-vcstool'
'python-colcon-common-extensions'
'python-empy'
'log4cxx'

'cmake'
'eigen'
'tinyxml2'
'asio'
'opencv'

'sip'
'python-sip'
'python-shiboken2'
'shiboken2'
'python-lxml'
)


source=(
'git+https://github.com/ros2/ros2#branch=crystal-release'
'rviz_common_args.patch'
)

sha256sums=(
SKIP
SKIP
)

options=(!strip)

prepare() {
	echo 'Getting Sources'

	[ -f /usr/lib/python3.8/site-packages/PySide2/__init__.py ] && echo 'pyside2 is installed. Can not build. Remove with "pacman -Rdd pyside2" (you can reinstall after the build)' && exit 1
	
	cd "$srcdir"
	mkdir -p src
	vcs import src < ros2/ros2.repos

	patch --forward -Np1 -i ./rviz_common_args.patch || true
}

build() {
	echo 'Building'
	cd "$srcdir"
	
	cxx_flags_selfish="-Wno-error=deprecated-copy \
		-Wno-error=deprecated-declarations \
		-O3 \
		-march=native \
		-mtune=native"

	cxx_flags_normal="-Wno-error=deprecated-copy \
		-Wno-error=deprecated-declarations \
		-O2 \
		-march=x86-64 \
		-mtune=generic"

	# pick one
	#cxx_flags="${cxx_flags_normal}"
	cxx_flags="${cxx_flags_selfish}"

	colcon build --merge --cmake-args \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DBUILD_TESTING=OFF \
		-DENABLE_TESTING=OFF \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		-DCMAKE_CXX_FLAGS="${cxx_flags}" \
	--packages-skip-by-dep qt_gui_cpp \
	--packages-skip qt_gui_cpp

	colcon build --merge --packages-select qt_gui_cpp \
		--cmake-args \
			-DCMAKE_BUILD_TYPE=RelWithDebInfo \
			-DBUILD_TESTING=OFF \
			-DENABLE_TESTING=OFF \
			-DCMAKE_VERBOSE_MAKEFILE=ON \
			-DCMAKE_CXX_FLAGS="${cxx_flags}" \
		--event-handlers console_cohesion+ console_direct+ || \
	echo "Manually compile troublesome qt unit" && \
	pushd $srcdir/build/qt_gui_cpp/sip/qt_gui_cpp_sip/ && \
	g++ -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -shared -Wl,-rpath,/usr/lib -Wl,--version-script="$srcdir/build/qt_gui_cpp/src/qt_gui_cpp_sip/libqt_gui_cpp_sip".exp -o "$srcdir/build/qt_gui_cpp/src/qt_gui_cpp_sip/libqt_gui_cpp_sip".so siplibqt_gui_cpp_sipcmodule.o siplibqt_gui_cpp_sipQList0101qt_gui_cppPluginProvider.o siplibqt_gui_cpp_sipQList0101qt_gui_cppPluginDescriptor.o siplibqt_gui_cpp_sipQMap0100QString0100QString.o siplibqt_gui_cpp_sipqt_gui_cppSettings.o siplibqt_gui_cpp_sipqt_gui_cppRosPluginlibPluginProvider_ForPlugins.o siplibqt_gui_cpp_sipqt_gui_cppRosPluginlibPluginProvider_ForPluginProviders.o siplibqt_gui_cpp_sipqt_gui_cppRecursivePluginProvider.o siplibqt_gui_cpp_sipqt_gui_cppPluginDescriptor.o siplibqt_gui_cpp_sipqt_gui_cppPluginContext.o siplibqt_gui_cpp_sipqt_gui_cppPluginBridge.o siplibqt_gui_cpp_sipqt_gui_cppPlugin.o siplibqt_gui_cpp_sipqt_gui_cppGenericProxy.o siplibqt_gui_cpp_sipqt_gui_cppCompositePluginProvider.o siplibqt_gui_cpp_sipqt_gui_cppPluginProvider.o siplibqt_gui_cpp_sipqt_gui_cpp.o -Llib -L/usr/lib -L/usr/X11R6/lib -lstdc++fs $srcdir/install/lib/libament_index_cpp.so $srcdir/install/lib/libclass_loader.so $srcdir/install/lib/librcutils.so $srcdir/install/lib/librcutils.so $srcdir/install/lib/librcutils.so /usr/lib/libtinyxml2.so /usr/lib/libpython3.8.so $srcdir/build/qt_gui_cpp/src/qt_gui_cpp/libqt_gui_cpp.a -lQt5Core -lpthread -lQt5Gui -lpthread -lQt5Widgets -lQt5PrintSupport -lXext -lX11 -lm -lpthread && \
	popd && \
	colcon build --merge --packages-select qt_gui_cpp \
		--cmake-args \
			-DCMAKE_BUILD_TYPE=RelWithDebInfo \
			-DBUILD_TESTING=OFF \
			-DENABLE_TESTING=OFF \
			-DCMAKE_VERBOSE_MAKEFILE=ON \
			-DCMAKE_CXX_FLAGS="${cxx_flags}" \
		--event-handlers console_cohesion+ console_direct+

	colcon build --merge --packages-above qt_gui_cpp \
		--cmake-args \
			-DCMAKE_BUILD_TYPE=RelWithDebInfo \
			-DBUILD_TESTING=OFF \
			-DENABLE_TESTING=OFF \
			-DCMAKE_CXX_FLAGS="${cxx_flags}"
	
	colcon build --merge \
		--cmake-args \
			-DCMAKE_BUILD_TYPE=RelWithDebInfo \
			-DBUILD_TESTING=OFF \
			-DENABLE_TESTING=OFF \
			-DCMAKE_CXX_FLAGS="${cxx_flags}"
}

package() {
	echo 'Packaging'
	mkdir -p $pkgdir/opt/ros/crystal

	echo "Patching PREFIX paths"
	repdir="${srcdir}/install"
	find . -iname "*.sh" -exec sed -i "s:${repdir}:/opt/ros/crystal:g" {} \;
	find . -iname "*.cmake" -exec sed -i "s:${repdir}:/opt/ros/crystal:g" {} \;

	cp -R $srcdir/install/. $pkgdir/opt/ros/crystal/.
}
